import * as FirebaseModule from 'firebase';
import firebaseConfig from '../constants/firebase';

const {
  apiKey,
  authDomain,
  databaseURL,
  storageBucket,
  messagingSenderId,
  projectId,
} = firebaseConfig;

let firebaseInitialized = false;

if (
  apiKey !== 'null'
  && authDomain !== 'null'
  && databaseURL !== 'null'
  && storageBucket !== 'null'
  && messagingSenderId !== 'null'
) {
  FirebaseModule.initializeApp({
    apiKey,
    authDomain,
    databaseURL,
    projectId,
    storageBucket,
    messagingSenderId
  });
  
  firebaseInitialized = true;
}

let FirebaseFires = firebaseInitialized ? FirebaseModule.firestore() : null;
let settings = { timestampsInSnapshots: true };
FirebaseFires.settings(settings);

export const FirebaseRef = firebaseInitialized ? FirebaseModule.database().ref() : null;

export const Firestore = FirebaseFires ? FirebaseFires : null;

export const Firebase = firebaseInitialized ? FirebaseModule : null;


