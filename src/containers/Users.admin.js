import React, {Component} from 'react';
import { MDBDataTable } from 'mdbreact';
import { getUsers } from '../actions/Users.action';
import { connect } from 'react-redux';

class UsersContainer extends Component {

  componentDidMount = () => this.props.onPageLoad();

  render(){
    const {
      users
    } = this.props;
    const columns = [
      {
        label: 'Name',
        field: 'name',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Position',
        field: 'position',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Office',
        field: 'office',
        sort: 'asc',
        width: 200
      },
      {
        label: 'Age',
        field: 'age',
        sort: 'asc',
        width: 100
      },
      {
        label: 'Start date',
        field: 'date',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Salary',
        field: 'salary',
        sort: 'asc',
        width: 100
      }
    ]
    const data = {
      columns,
      rows: Object.values(users.data)
    }

    console.log(data);
    return (
      <MDBDataTable
        striped
        hover
        data={data}
      />
    );
  }
  
  
}

const mapStateToProps = state => ({
  users: state.users || {}
});

const mapDispatchToProps = {
  onPageLoad: getUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);