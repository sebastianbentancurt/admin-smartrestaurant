import React, {Component} from 'react';
import LoginComponent from '../components/Login/Login.component';
import { login } from '../actions/Login.action';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { normalize, schema } from 'normalizr';

class LoginContainer extends Component {
    
    static propTypes = {
        member: PropTypes.shape({}).isRequired,
        onFormSubmit: PropTypes.func.isRequired,
        isLoading: PropTypes.bool.isRequired,
        successMessage: PropTypes.string.isRequired,
      }
    state = {
        errorMessage: null,
    }
    
    onFormSubmit = (email,password) => {
        const { onFormSubmit } = this.props;
        
        return onFormSubmit(email,password)
          .catch((err) => { this.setState({ errorMessage: err }); throw err; });
      }
    render() {
        const {
            member,
            isLoading,
            successMessage,
          } = this.props;
      
        const { errorMessage } = this.state;

        return (
            <LoginComponent 
                member={member}
                loading={isLoading}
                error={errorMessage}
                success={successMessage}
                onFormSubmit={this.onFormSubmit}     
            />
        );
    }
}

const mapStateToProps = state => ({
    member: state.member || {},
    isLoading: state.status.loading || false,
    successMessage: state.status.success || '',
  });

const mapDispatchToProps = {
    onFormSubmit: login,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);