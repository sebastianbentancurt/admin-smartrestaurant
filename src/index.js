/* global document */
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Route } from 'react-router-dom';
import configureStore from './store/index';
import registerServiceWorker from './register-service-worker';
import Routes from './routes';

// Components
//import Loading from './components/Loading';

// Load css
require('./styles/app.scss');

const { persistor, store } = configureStore();
// persistor.purge(); // Debug to clear persist

const rootElement = document.getElementById('app-site');

const Root = () => (
  <Provider store={store}>
    <PersistGate  persistor={persistor}>
      <Router>
            <Routes />
      </Router>
    </PersistGate>
  </Provider>
);

render(<Root />, rootElement);
registerServiceWorker();
