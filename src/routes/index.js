import React from 'react';
import { Switch, Route } from 'react-router-dom';

import SignIn from '../containers/Login.container';
import TemplateMain from '../components/Templates/TemplateMain'
import TemplateClean from '../components/Templates/TemplateClean';
import PrivateRoute from '../components/PrivateRoute';
import Users from '../containers/Users.admin';

const Index = () => (
    
    <Switch>
        <PrivateRoute exact path="/" component={props => (
            <TemplateMain>
                <Users></Users>
            </TemplateMain>
        )}/>
        <Route path="/sign-in" render={props => (
            <TemplateClean>
                <SignIn {...props} />
            </TemplateClean>
        )}/>
    </Switch>
)

export default Index;