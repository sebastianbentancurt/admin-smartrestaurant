// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux';
import { isLoggedIn } from '../actions/Login.action';

const PrivateRoute = ({ component: Component, ...rest }) => {

  // Add your own authentication on the below line.
  var isLoggedI = isLoggedIn();
  console.log(isLoggedI);
  return (
    <Route
      {...rest}
      render={props =>
        isLoggedI ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/sign-in', state: { from: props.location } }} />
        )
      }
    />
  )
}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  isLoggedIn: isLoggedIn,
};


export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);