import React from 'react';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import Button from 'material-ui/Button';
import Notifier, { openSnackbar } from 'components/Notifier';
import {FormControlLabel} from 'material-ui/Form';
import PropTypes from 'prop-types';
import logo from '../../assets/logo.png';
import { withRouter } from 'react-router-dom';

class LoginComponent extends React.Component {
    
    static defaultProps = {
        success: null,
        error: null,
        member: {},
    }
    
    static propTypes = {
        member: PropTypes.shape({
          email: PropTypes.string,
        }),
        error: PropTypes.string,
        success: PropTypes.string,
        loading: PropTypes.bool.isRequired,
        onFormSubmit: PropTypes.func.isRequired,
        history: PropTypes.shape({
          push: PropTypes.func.isRequired,
        }).isRequired,
      }
      
    constructor(props) {
        super(props);
        this.state = {
            email: (props.member && props.member.email) ? props.member.email : '',
            password: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    

    
    handleSubmit(event) {
        event.preventDefault();
        const { onFormSubmit, history } = this.props;
        const { email, password } = this.state;
        onFormSubmit(email, password)
        .then(() => history.push('/'))
        .catch(e => console.log(`Error: ${e}`));
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }
    render() { 
        const { loading, success, error } = this.props;
        const { email, password } = this.state;

      return (
        <div
            className="login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
            <div className="login-content">
                <div className="login-header mb-4" style={{textAlign:'center'}} >
                    <a className="app-logo" href="#/" title="Jambo">
                        <img src={logo} alt="jambo" title="jambo"/>
                    </a>
                </div>
                <Notifier />
                {!!success && (
                    openSnackbar({ message: success })
                )}
                {!!error && (
                    openSnackbar({ message: error })
                )}
                
                <div className="login-form">
                    <form>
                        <fieldset>
                            <TextField
                                name="email"
                                label="Email"
                                fullWidth
                                value={email}
                                margin="normal"
                                className="mt-1"
                                onChange={this.handleChange}
                            />
                            <TextField
                                type="password"
                                name="password"
                                label="Password"
                                value={password}
                                fullWidth
                                margin="normal"
                                className="mt-1"
                                onChange={this.handleChange}
                            />
                            <div className="mt-1 mb-2 d-flex justify-content-between align-items-center">
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            value="gilad"
                                        />
                                    }
                                    label="Remember me"
                                />

                                <div>
                                    <a className="text-primary" href="#/app/app-module/forgot-password-2"
                                       title="Reset Password">Forgot
                                        your password</a>
                                </div>
                            </div>

                            <Button color="primary" onClick={this.handleSubmit}
                                    href="javascript:void(0)" variant="raised"
                                    className="jr-btn text-white btn-primary">Sign
                                In</Button>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
      )
    }
  }

export default withRouter(LoginComponent);
