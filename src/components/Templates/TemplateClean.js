import React from 'react';

import Header from 'components/Header/index';
import Sidebar from 'components/SideNav/index';
import Footer from 'components/Footer';

import {connect} from 'react-redux';
import {isIOS, isMobile} from 'react-device-detect';

import {COLLAPSED_DRAWER, FIXED_DRAWER} from 'constants/ActionTypes';
import {toggleCollapsedNav} from 'actions/index';
import defaultTheme from './themes/defaultTheme';
import 'react-big-calendar/lib/less/styles.less';
import 'styles/bootstrap.scss'
import 'styles/app.scss';
import {createMuiTheme, MuiThemeProvider} from 'material-ui/styles';

class TemplateMain extends React.Component {
    
    onToggleCollapsedNav = (e) => {
      const val = !this.props.navCollapsed;
      this.props.toggleCollapsedNav(val);
    };

  
    render () {
      
      const {drawerType} = this.props;
      const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';
      //set default height and overflow for iOS mobile Safari 10+ support.
      if (isIOS && isMobile) {
          $('#body').addClass('ios-mobile-view-height')
      }
      else if ($('#body').hasClass('ios-mobile-view-height')) {
          $('#body').removeClass('ios-mobile-view-height')
      }

      const { children } = this.props;

      return (
        <MuiThemeProvider theme={createMuiTheme(defaultTheme)}>
            <div className="app-main">
                <div className={`app-container ${drawerStyle}`}>
                    <div className="app-main-container">
                        <div className="app-header">
                        </div>

                        <main className="app-main-content-wrapper">
                            <div className="app-main-content">
                            {children}
                            </div>
                            <Footer/>
                        </main>
                    </div>
                </div>
            </div>
        </MuiThemeProvider>
        )
    }
  }

const mapStateToProps = ({settings}) => {
    const {navCollapsed, drawerType, sideNavColor} = settings;
    return {navCollapsed, drawerType, sideNavColor}
};

export default connect(mapStateToProps, {toggleCollapsedNav})(TemplateMain);
