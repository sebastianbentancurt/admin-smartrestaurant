import { normalize, schema } from 'normalizr';

export const initialState = {
    loading: false,
    data: [],
    error: null,
    success: null,
  };
  
  export default function appReducer(state = initialState, action) {
    switch (action.type) {
      case 'USERS_REPLACE': {
        return {
          ...state,
          loading: action.loading || false,
          data: action.data || null,
          error: action.error || null,
          success: action.success || null,
        };
      }
      default:
        return state;
    }
  }
  

// Define a users schema
const user = new schema.Entity('users');

// Define your comments schema
const comment = new schema.Entity('comments', {
  commenter: user
});

// Define your article
const article = new schema.Entity('articles', {
  author: user,
  comments: [comment]
});
