import status from './status'
import settings from './settings'
import member from './member'
import users from './users'

const rehydrated = (state = false, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE':
      return true;
    default:
      return state;
  }
};

export default {
  rehydrated,
  status,
  settings,
  member,
  users
};

