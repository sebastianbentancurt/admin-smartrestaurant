import { Firebase, Firestore } from '../lib/firebase';
import statusMessage from './Status.action';
import { normalize } from 'normalizr';

export function getUsers() {
  if (Firestore === null) return () => new Promise(resolve => resolve());

  return dispatch => new Promise(resolve => Firestore.collection('users')
  .get()
  .then((snapshot) => {
    const data = snapshot.docs.map(doc => doc.data()) || {};
    const normalizedData = normalize(data, user);
    console.log(normalizedData)
    //listAllUsers();
    return resolve(dispatch({
      type: 'USERS_REPLACE',
      data: {...data},
    }));
  })
  .catch(e => console.log(e))
  
  );
}

function listAllUsers() {
  // List batch of users, 1000 at a time.
  Firebase.auth().listUsers(1000, 1)
    .then(function(listUsersResult) {
      listUsersResult.users.forEach(function(userRecord) {
        console.log("user", userRecord.toJSON());
      });
      if (listUsersResult.pageToken) {
        // List next batch of users.
        listAllUsers(listUsersResult.pageToken)
      }
    })
    .catch(function(error) {
      console.log("Error listing users:", error);
    });
}

