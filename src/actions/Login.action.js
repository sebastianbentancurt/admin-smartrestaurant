import { Firebase, FirebaseRef, Firestore } from '../lib/firebase';
import statusMessage from './Status.action';

export function login(email, password) {
    if (Firebase === null) return () => new Promise(resolve => resolve());

    return dispatch => new Promise(async (resolve, reject) => {
      await statusMessage(dispatch, 'loading', true);
  
      // Go to Firebase
      return Firebase.auth()
        .setPersistence(Firebase.auth.Auth.Persistence.LOCAL)
        .then(() => Firebase.auth()
          .signInWithEmailAndPassword(email, password)
          .then(async (res) => {
            const userDetails = res && res.user ? res.user : null;
  
            if (userDetails.uid) {
              // Update last logged in data
              Firestore.collection('users').doc(`${userDetails.uid}`).update({
                lastLoggedIn: Firebase.firestore.FieldValue.serverTimestamp(),
              });
  
              // Send verification Email when email hasn't been verified
              if (userDetails.emailVerified === false) {
                Firebase.auth().currentUser
                  .sendEmailVerification()
                  .catch(() => console.log('Verification email failed to send'));
              }
              
              // Get User Data
              getUserData(dispatch);
            }
  
            await statusMessage(dispatch, 'loading', false);
  
            // Send Login data to Redux
            return resolve(dispatch({
              type: 'USER_LOGIN',
              data: userDetails,
            }));
          }).catch(reject));
    }).catch(async (err) => {
      await statusMessage(dispatch, 'loading', false);
      throw err.message;
    });
}

/**
  * Logout
  */
 export function logout() {
  return dispatch => new Promise((resolve, reject) => {
    Firebase.auth().signOut()
      .then(() => {
        dispatch({ type: 'USER_RESET' });
        setTimeout(resolve, 1000); // Resolve after 1s so that user sees a message
      }).catch(reject);
  }).catch(async (err) => { await statusMessage(dispatch, 'error', err.message); throw err.message; });
}

/**
  * Get this User's Details
  */
 function getUserData(dispatch) {
  const UID = (
    Firestore
    && Firebase
    && Firebase.auth()
    && Firebase.auth().currentUser
    && Firebase.auth().currentUser.uid
  ) ? Firebase.auth().currentUser.uid : null;

  if (!UID) return false;

  const fire = Firestore.collection('users').doc(`${UID}`);

  return fire.get()
  .then((snapshot) => {

    const userData = snapshot.data() || [];

    return dispatch({
      type: 'USER_DETAILS_UPDATE',
      data: userData,
    });
  });
}

export function getMemberData() {
  if (Firebase === null) return () => new Promise(resolve => resolve());

  // Ensure token is up to date
  return dispatch => new Promise((resolve) => {
    Firebase.auth().onAuthStateChanged((loggedIn) => {
      if (loggedIn) {
        return resolve(getUserData(dispatch));
      }

      return () => new Promise(() => resolve());
    });
  });
}

export function isLoggedIn() {
  if (Firebase === null) return () => new Promise(resolve => resolve());

  // Ensure token is up to date
  return dispatch => new Promise((resolve) => {
    Firebase.auth().onAuthStateChanged((loggedIn) => {
      if (loggedIn) {
        return resolve(true);
      }

      return () => new Promise(() => resolve(false));
    });
  });
}

